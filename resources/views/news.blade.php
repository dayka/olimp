@include('app\header')
<div class="container news-all">
    <div class="gal-tit mt-4">{{ __('base.news') }}</div>
    <div class="row mt-4 news-card">
        @foreach ($newss as $news)
        <div class="col-lg-3 col-md-6 p-0">
            <div class="news-card shadow mb-25">
            <a class="text-decoration-none d-block" href="{{ route('news.frontShow', $news) }}">
            <div class="news-card-image">
                <img src="{{ asset('admin_assets/images/' . $news->image) }}">
            </div>
            <div class="news-caption">
                <div class="news-title">{{  \Illuminate\Support\Str::limit($news->title, 100, $end='...')  }}</div>
                <div class="news-text">{!! \Illuminate\Support\Str::limit($news->description, 200, $end='...') !!}</div>
                <div class="news-date">{{ date_format($news->created_at,'d.m.Y') }}</div>
            </div>
            </a>
        </div>
        </div>
        @endforeach
    </div>
</div>
@include('app.footer')
