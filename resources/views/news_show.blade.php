@include('app\header')
<div class="container-md news-all">
    <div class="news-show-breadcrumb">
        <div class="br-cr-item me-2"><a class="text-decoration-none" href="{{ route('home') }}">{{ __('base.home') }}</a></div>
        <div class="br-cr-icon">></div>
        <div class="br-cr-item mx-2"><a class="text-decoration-none" href="{{ route('news.frontIndex') }}">{{ __('base.news') }}</a></div>
        <div class="br-cr-icon">></div>
        <div class="br-cr-item ms-2">{{ $news->title }}</div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="news-show-image mb-3">
                <img src="{{ asset('admin_assets/images/' . $news->image) }}" alt="">
            </div>
            <div class="news-show-date">{{ date_format($news->created_at,'d.m.Y') }}</div>
            <div class="news-show-title">{{ $news->title }}</div>
            <div class="news-show-text">{!! $news->description !!}</div>
        </div>
        <div class="col-md-4">
            <div class=" border-start ps-4" style="border-left-color: black!important">
                <div class="news-show-all">{{ __('base.latest-news') }}</div>
                @foreach ($all_news as $news)
                <div class="news-all-item">
                    <div class="news-un-la-date">{{ date_format($news->created_at,'d.m.Y') }}</div>
                    <div class="news-show-text mt-10">{{ $news->title }}</div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
{{--  <div class="news-show-main mt-150">
    <div class="news-show-breadcrumb">
        <div class="br-cr-item">Baş sahypa</div>
        <div class="br-cr-icon">></div>
        <div class="br-cr-item">Habarlar</div>
        <div class="br-cr-icon">></div>
        <div class="br-cr-item">Lorem ipsum dolor sit amet consectetur.</div>
    </div>
    <div class="news-show">
        <div class="news-show-left">
            <div class="news-show-image">
                <img src="{{ asset('images/news_show.jpg') }}">
            </div>
            <div class="news-show-caption">
                <div class="news-show-header">
                    <div class="news-show-date">12.12.2022</div>
                    <div class="news-show-title">Lorem ipsum dolor sit amet consectetur. Mattis phasellus in interdum ullamcorper.</div>
                    <div class="news-show-text">
                                  Lorem ipsum dolor sit amet consectetur. Tempor felis mattis bibendum mattis at euismod porttitor libero. Ipsum facilisi ac accumsan non sed. Eleifend libero arcu aenean quis. Viverra id pharetra est mattis cras malesuada dolor. Ut senectus lorem nulla rhoncus. Faucibus mi neque aliquam posuere nibh gravida.

      Sapien tempor nunc at adipiscing dolor. Volutpat elementum velit lacinia in at vel arcu sed morbi. Feugiat nibh tristique nec sagittis nisl ipsum. Accumsan ut urna pellentesque pharetra sem. Amet iaculis maecenas odio laoreet accumsan cursus turpis sed. Est vel risus in auctor praesent aenean non vulputate. Sed mattis a accumsan adipiscing justo nulla eget diam praesent. Tellus imperdiet libero nam tellus sagittis diam vitae est consectetur. Bibendum massa habitant enim ipsum nisi.

      Magna auctor fames at pellentesque nunc ipsum risus. Arcu neque cursus nullam egestas in. Elit volutpat tempus cursus cursus etiam. Cursus a mi mi quam pretium volutpat. Mi neque dolor mattis non vestibulum enim fermentum. Pellentesque purus pellentesque urna elementum quisque lacus sed. Nam ac mi sed tellus aenean tempus vitae vel nibh. Nunc ut imperdiet senectus arcu leo. Elit cras condimentum sit consequat feugiat nullam.

      Elementum aliquet tristique quisque nascetur porttitor et. Leo in non quisque at. Etiam vulputate sagittis convallis tellus adipiscing tempus nisi. Elementum diam aliquet amet tortor magna ultricies congue morbi. Gravida arcu nibh imperdiet quis tempus id quam donec justo. Lectus sit eget auctor suspendisse enim dignissim. Sed aliquam nunc at eget feugiat et in tortor. Id enim at bibendum ligula vitae at diam ac. Euismod nam commodo est fermentum quis dui orci. Id volutpat proin at suscipit.
                    </div>
                </div>
            </div>
        </div>
        <div class="news-show-right">
            <div class="news-show-all">Похожие новости</div>
            <div class="news-all-item">
                <div class="news-date">12.12.2022</div>
                <div class="news-show-text mt-10">Lorem ipsum dolor sit amet consectetur. Mattis phasellus in interdum ullamcorper.</div>
            </div>
            <div class="news-all-item">
                <div class="news-date">12.12.2022</div>
                <div class="news-show-text mt-10">Lorem ipsum dolor sit amet consectetur. Mattis phasellus in interdum ullamcorper.</div>
            </div>
            <div class="news-all-item">
                <div class="news-date">12.12.2022</div>
                <div class="news-show-text mt-10">Lorem ipsum dolor sit amet consectetur. Mattis phasellus in interdum ullamcorper.</div>
            </div>
            <div class="news-all-item">
                <div class="news-date">12.12.2022</div>
                <div class="news-show-text mt-10">Lorem ipsum dolor sit amet consectetur. Mattis phasellus in interdum ullamcorper.</div>
            </div>
            <div class="news-all-item">
                <div class="news-date">12.12.2022</div>
                <div class="news-show-text mt-10">Lorem ipsum dolor sit amet consectetur. Mattis phasellus in interdum ullamcorper.</div>
            </div>
            <div class="news-all-item mb-0">
                <div class="news-date">12.12.2022</div>
                <div class="news-show-text mt-10">Lorem ipsum dolor sit amet consectetur. Mattis phasellus in interdum ullamcorper.</div>
            </div>
        </div>
    </div>
</div>  --}}
@include('app.footer')
