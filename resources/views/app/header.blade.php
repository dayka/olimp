<!DOCTYPE html>
<html>
<head>
	<title>News</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>
	<div class="container-fluid top-nav bg-news z-1">
        <div class="container">
            <nav class="navbar navbar-expand-md bg-transparent">
                <div class="container-fluid p-0 align-items-center">
                    <a class="navbar-brand align-items-center me-0 p-0" href="{{ route('home') }}">
                        <div class="container p-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <div class="top-nav-logo-image">
                                        <img src="{{ asset('images/image-by-item-and-alias 1.png') }}">
                                    </div>
                                </div>
                                <div class="col ps-0">
                                    <div class="top-nav-caption">{!! __('base.logo') !!}</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse flex-grow-1 justify-content-center align-items-baseline" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link py-0 active text-white" aria-current="page" href="{{ route('home') }}">{{ __('base.home') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0 text-white" href="#">{{ __('base.us') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0 text-white" href="#">{{ __('base.news') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0 text-white" href="#">{{ __('base.gallery') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0 text-white con me-0" id="top-contact" href="#">{{ __('base.contact') }}</a>
                            </li>
                        </ul>
                        <div class="lang d-flex">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <a style="margin-right: {{ $loop->last ? 'unset' : '20px' }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                <div class="lang-image">
                                    <img src="{{ asset('images/' . $localeCode . '.svg') }}">
                                </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
