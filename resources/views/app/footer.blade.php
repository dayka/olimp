<div class="container-fluid footer py-3">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 d-flex align-items-center" id="footer-logo">
          <img class="footer-logo-image me-3" src="{{ asset('images/image-by-item-and-alias 1.png') }}">
          <div class="footer-logo-text">{!! __('base.logo') !!}</div>
        </div>
    </div>
    <div class="row justify-content-center mt-4">
      <div class="col-xl-4 col-lg-5 col-md-7 col-sm-9 text-center align-items-center" id="footer-menu">
          <a class="footer-menu-item text-decoration-none" href="#">{{ __('base.home') }}</a>
        <a class="footer-menu-item text-decoration-none" href="#">{{ __('base.us') }}</a>
        <a class="footer-menu-item text-decoration-none" href="{{ route('news.frontIndex') }}">{{ __('base.news') }}</a>
        <a class="footer-menu-item me-0 text-decoration-none" href="#">{{ __('base.gallery') }}</a>
      </div>
    </div>
    <div class="row justify-content-center mt-4">
      <div class="col-xxl-7 col-lg-9 align-items-center" id="footer-about">
        <div class="footer-about-item" href="#">{{ __('base.phone') }}</div>
        <div class="footer-about-item" href="#">{{ __('base.fax') }}</div>
        <div class="footer-about-item" href="#">{{ __('base.email') }}</div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col text-center"><div class="footer-copyright mb-0">{{ __('base.copyright') }}</div></div>
    </div>
    </div>
  </div>
  <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
</body>
</html>
