<!DOCTYPE html>
<html>
<head>
  <title>Sport</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>
    <div class="container-fluid top-nav z-1">
        <div class="container">
            <nav class="navbar navbar-expand-md bg-transparent">
                <div class="container-fluid p-0 align-items-center">
                    <a class="navbar-brand align-items-center me-0 p-0" href="{{ route('home') }}">
                        <div class="container p-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <div class="top-nav-logo-image">
                                        <img src="images/image-by-item-and-alias 1.png">
                                    </div>
                                </div>
                                <div class="col ps-0">
                                    <div class="top-nav-caption">{!! __('base.logo') !!}</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse flex-grow-1 justify-content-center align-items-baseline" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link py-0 active text-white" aria-current="page" href="{{ route('home') }}">{{ __('base.home') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0 text-white" href="#">{{ __('base.us') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0 text-white" href="{{ route('news.frontIndex') }}">{{ __('base.news') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0 text-white" href="#">{{ __('base.gallery') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0 text-white con me-0" id="top-contact" href="#">{{ __('base.contact') }}</a>
                            </li>
                        </ul>
                        <div class="lang d-flex">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <a style="margin-right: {{ $loop->last ? 'unset' : '20px' }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                <div class="lang-image">
                                    <img src="images/{{ $localeCode }}.svg">
                                </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="container-fluid px-0">
        <div id="carouselExampleAutoplaying" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/1 ß½á¬ñ.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption large-caption">
                        <div class="text-carousel fw-bold text-start">{{ __('base.carousel-title') }}</div>
                    </div>
                    <div class="carousel-caption small-caption col d-flex">
                        <div class="first_carousel_logo">
                            <img src="images/logo 23 1.svg">
                        </div>
                        <div class="first_carousel_logo_text col-6 text-start ms-2 mt-1">{{ __('base.main-title') }}</div>
                    </div>
                </div>
                {{--  <div class="carousel-item">
                    <img src="images/2 ß½á¬ñ.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption second-caption d-flex align-items-center">
                        <div class="second_carousel_logo">
                            <img src="images/logo 23 1.svg">
                        </div>
                        <div class="col-6 text-uppercase text-white fw-bold text-start second_carousel_caption ms-2">{{ __('base.main-title') }}</div>
                    </div>
                </div>  --}}
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-6" id="flip_one">
                <div class="flip-card">
                    <div class="flip-card-image">
                        <img src="images/flip_card_1.jpg">
                    </div>
                    <div class="flip-card-caption">
                        <div class="flip-card-text">{{ __('base.flip-1-name') }}</div>
                    </div>
                    <div class="flip-card-caption-second p-3">
                        <div class="flip-card-second-title">{{ __('base.flip-1-name') }}</div>
                    <div class="flip-card-second-text">{{ __('base.flip-1-text') }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="flip-card me-0">
                    <div class="flip-card-image">
                        <img src="images/flip_card_2.jpg">
                    </div>
                    <div class="flip-card-caption">
                        <div class="flip-card-text">{{ __('base.flip-2-name') }}</div>
                    </div>
                    <div class="flip-card-caption-second p-3 container">
                        <div class="flip-card-second-title">{{ __('base.flip-2-name') }}</div>
                        <div class="row">
                            <div class="col-8">
                                <div class="flip-card-mini-title">{{ __('base.flip-2-mini-title-1') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-1') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-2') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-3') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-4') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-5') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-6') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-7') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-8') }}</div>
                                <div class="flip-card-second-text">{{ __('base.flip-2-text-9') }}</div>
                            </div>
                            <div class="col-4">
                                <div class="flip-card-mini-title">{{ __('base.flip-2-mini-title-2') }}</div>
                                <div class="flip-card-second-text">100</div>
                                <div class="flip-card-second-text">100</div>
                                <div class="flip-card-second-text">100</div>
                                <div class="flip-card-second-text">100</div>
                                <div class="flip-card-second-text">100</div>
                                <div class="flip-card-second-text">100</div>
                                <div class="flip-card-second-text">100</div>
                                <div class="flip-card-second-text">100</div>
                                <div class="flip-card-second-text">100</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-4 mb-5">
        <div class="row">
            <div class="col-lg-6" id="flip_three">
                <div class="flip-card">
                    <div class="flip-card-image">
                        <img src="images/flip_card_3.jpg">
                    </div>
                    <div class="flip-card-caption">
                        <div class="flip-card-text">{{ __('base.flip-3-name') }}</div>
                    </div>
                    <div class="flip-card-caption-second p-3 container">
                        <div class="flip-card-second-title">{{ __('base.flip-3-name') }}</div>
                        <div class="flip-card-mini-title">{{ __('base.flip-3-mini-title-1') }}</div>
                        <div class="flip-card-second-text">{{ __('base.flip-3-text-1') }}</div>
                        <div class="flip-card-mini-title">{{ __('base.flip-3-mini-title-2') }}</div>
                        <div class="flip-card-second-text">{{ __('base.flip-3-text-2') }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
              <div class="flip-card me-0">
                  <div class="flip-card-image">
                      <img src="images/flip_card_4.jpg">
                  </div>
                  <div class="flip-card-caption">
                      <div class="flip-card-text">{{ __('base.flip-4-name') }}</div>
                  </div>
                  <div class="flip-card-caption-second p-3 container">
                      <div class="flip-card-second-title">{{ __('base.flip-4-name') }}</div>
                      <div class="flip-card-mini-title">{{ __('base.flip-4-mini-title-1') }}</div>
                      <div class="flip-card-second-text">{{ __('base.flip-4-text-1') }}</div>
                      <div class="flip-card-mini-title">{{ __('base.flip-4-mini-title-2') }}</div>
                      <div class="flip-card-second-text">{{ __('base.flip-4-text-2') }}</div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid un-flip py-5">
        <div class="container">
            <div class="un-title">{{ __('base.flip-1-name') }}</div>
            <div class="row mt-5">
                <div class="col-lg-4 col-md-6" id="one">
                <div class="un-fl-tr-image">
                <img src="images/un_fl_tr_1.jpg">
                </div>
                </div>
                <div class="col-lg-4 col-md-6" id="two">
                <div class="un-fl-tr-image">
                <img src="images/un_fl_tr_2.jpg">
                </div>
                </div>
                <div class="col-lg-4 col-md-6" id="three">
                <div class="un-fl-tr-image">
                <img src="images/un_fl_tr_3.jpg">
                </div>
                </div>
            </div>
            <div class="un-text text-white mt-4 text-center">{{ __('base.large-text') }}</div>
        </div>
    </div>
    <div class="container my-5">
        <div class="gal-tit">{{ __('base.fotogallery') }}</div>
        <div class="row mt-4 gal-sma">
        <div class="col-lg-3 col-md-6">
            <div class="gal-image">
            <img src="images/gal_image.png">
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="gal-image">
            <img src="images/gal_image.png">
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="gal-image">
            <img src="images/gal_image.png">
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="gal-image me-0">
            <img src="images/gal_image.png">
            </div>
        </div>
        </div>
        <div class="row mt-4 gal-lar">
        <div class="col-xl-4 col-lg-6">
            <div class="gal-image">
            <img src="images/gal_image.png">
            </div>
        </div>
        <div class="col-xl-4 col-lg-6">
            <div class="gal-image">
            <img src="images/gal_image.png">
            </div>
        </div>
        <div class="col-xl-4 col-lg-6">
            <div class="gal-image">
            <img src="images/gal_image.png">
            </div>
        </div>
        </div>
        <div class="row mt-4 gal-sma">
            <div class="col-lg-3 col-md-6">
                <div class="gal-image">
                    <img src="images/gal_image.png">
                </div>
            </div>
        <div class="col-lg-3 col-md-6">
            <div class="gal-image">
            <img src="images/gal_image.png">
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="gal-image">
            <img src="images/gal_image.png">
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="gal-image me-0">
            <img src="images/gal_image.png">
            </div>
        </div>
        </div>
    @yield('content')
    <div class="row justify-content-center">
      <div class="col text-center all-news-name">
      <a class="text-decoration-none" href="{{ route('news.frontIndex') }}">{{ __('base.all-news') }}</a>
      </div>
    </div>
    </div>
</div>
  @include('app.footer')
