<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\CustomAuthController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group([
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function()
{
    Route::get('/', [HomeController::class, 'home'])->name('home');
    Route::get('newss', [NewsController::class, 'frontIndex'])->name('news.frontIndex');
    Route::get('newss/{news}', [NewsController::class, 'frontShow'])->name('news.frontShow');
}
);

Route::get('login', [CustomAuthController::class, 'login'])->name('auth.login');
Route::post('login-user/', [CustomAuthController::class, 'loginUser'])->name('auth.login.user');
Route::get('logout/', [CustomAuthController::class, 'logout'])->name('auth.logout');

Route::group(['prefix' => 'panel', 'middleware' => 'auth'], function()
{
    Route::resource('news', NewsController::class);
});