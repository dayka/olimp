<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [];

        if($this->method() == 'POST'){
            $rules = [
                'title' => 'required|array|size:3',
                'description' => 'required|array|size:3',
                'title.tk' => 'required',
                'title.ru' => 'required',
                'title.en' => 'required',
                'description.tk' => 'required',
                'description.ru' => 'required',
                'description.en' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
            ];
        } else {
            $rules = [
                'title' => 'required|array|size:3',
                'description' => 'required|array|size:3',
                'title.tk' => 'required',
                'title.ru' => 'required',
                'title.en' => 'required',
                'description.tk' => 'required',
                'description.ru' => 'required',
                'description.en' => 'required',
                'image' => 'nullable'
            ];
        }

        return $rules;
    }
}