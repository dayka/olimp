<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use Illuminate\Support\Str;
use App\Models\News;

class NewsController extends Controller
{
    public function frontIndex()
    {
        $newss = News::latest()->get();

        return view('news', compact('newss'));
    }

    public function frontShow(News $news)
    {
        $all_news = News::latest()->get();

        return view('news_show', compact('news', 'all_news'));
    }

    public function index()
    {
        $newss = News::latest()->get();

        return view('admin.news', compact('newss'));
    }

    public function create()
    {
        return view('admin.create');
    }

    public function store(NewsRequest $request)
    {
        $attributes = $request->validated();

        if($request->hasFile('image')){
            $image = $request->file('image');
            $destinationPath = public_path('admin_assets/images');
            $profileImage = Str::random(30) . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $attributes['image'] = $profileImage;
        }

        News::create($attributes);

        return redirect()->route('news.index')
            ->with('success','Product created successfully.');
    }

    public function edit(News $news)
    {
        return view('admin.edit', compact('news'));
    }

    public function update(News $news, NewsRequest $request)
    {
        $attributes = $request->validated();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            if(file_exists(public_path('admin_assets/images/' . $news->image))){
                unlink(public_path('admin_assets/images/' . $news->image));
            }
            $destinationPath = public_path('admin_assets/images');
            $profileImage = Str::random(30) . '.' . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $attributes['image'] = $profileImage;
        }else{
            unset($attributes['image']);
        }

        $news->update($attributes);

        return redirect()->route('news.index')
            ->with('success','Product updated successfully');

    }

    public function destroy(News $news)
    {
        if (file_exists(public_path('admin_assets/images/' . $news->image))) {
            unlink(public_path('admin_assets/images/' . $news->image));
        }

        $news->delete();

        return redirect()->route('news.index')
            ->with('success','Product deleted successfully');
    }
}