<?php

namespace App\Http\Controllers;

use App\Models\News;

class HomeController extends Controller
{
    public function home()
    {
        $newss = News::latest()->get();

        return view('home', compact('newss'));
    }
}
