<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class CustomAuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function loginUser(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'password'=>'required|min:5|max:12',
        ]);

        $credentials = $request->except(['_token']);

        if (auth()->attempt($credentials)) {

            return redirect()->route('news.index')
                ->with('success', 'You are logged in successfully !');

        }else{
            session()->flash('message', 'Invalid credentials');
            return redirect()->back();
        }
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('auth.login');
    }
}